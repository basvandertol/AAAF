from pylab import *
import scipy.linalg



# TODO

# process arguments

# open ms

# open coefficients

# compute filter

#    np.inv is broadcastable

# apply filter

#    np.einsum()






t = pyrap.tables.table('/var/scratch/bvdtol/apertif/WSRTA7101205_B000.MS/')

t1 = t.query("ANTENNA1 == ANTENNA2")

d0 = t1.getcol("DATA")[:,:,0]

d0 = numpy.sum(d0, axis = 0)

plot(abs(d0), '.-')

Nsb = 1024;     # number of subbands
Nch = 64;       # number of channels per subband

# load filter coefficients and pad with zeros

FIRcoeff = numpy.zeros(Nsb *  Nch )
cf = open("Coeffs16384Kaiser-quant.dat")
for i, coeff in enumerate(cf):
    FIRcoeff[i] = int(coeff)
FIRspec = abs(fft(FIRcoeff))**2;
FIRspec = FIRspec * Nch / sum(FIRspec);
FIRspec = numpy.roll(FIRspec, int(1.5*Nch))[:3*Nch].reshape((3, Nch))

Ainv_list = []
for ch in range(Nch):
    print ch
    row = numpy.zeros(Nsb)
    col = numpy.zeros(Nsb)
    row[0] = col[0] = FIRspec[1, ch]
    #row[1] = FIRspec[2, ch]
    #col[1] = FIRspec[0, ch]
    A = scipy.linalg.toeplitz(row, col)
    Ainv = numpy.linalg.inv(A)
    Ainv_list.append(Ainv)

d = d0.copy()

for ch in range(Nch):
    d[ch::Nch] = numpy.dot(Ainv_list[ch][:172,:172], d[ch::Nch])

plot(abs(d),'.-')


Ainv_list = []
for ch in range(Nch):
    print ch
    row = numpy.zeros(Nsb)
    col = numpy.zeros(Nsb)
    row[0] = col[0] = FIRspec[1, ch]
    row[1] = FIRspec[2, ch]
    col[1] = FIRspec[0, ch]
    A = scipy.linalg.toeplitz(row, col)
    Ainv = numpy.linalg.inv(A)
    Ainv_list.append(Ainv)

d = d0.copy()

for ch in range(Nch):
    d[ch::Nch] = numpy.dot(Ainv_list[ch][:172,:172], d[ch::Nch])

plot(abs(d),'.-')




##N1 = 1024

##f = array([1])

##for k in range(1,N):
   ##a = array( [-exp(1j*2*pi/N1*k), 1])
   ##f = convolve(f,a)
   
##f = ones((N1,))
   
##N = 16*1024


##plot(20*log10(abs(fftshift(fft(fftshift(concatenate((z,f,z))))))/N1))
##ylim(-150,5)

##xlim(N/2-10, N/2+10)

#cf = open("Coeffs16384Kaiser-quant.dat")
#coeffs = [int(coeff) for coeff in cf]
#a = array(coeffs)

#plot(a)



#oversample = 16
#z = zeros((0.5*(oversample-1)*len(a),))
##z = array([])

#r = fftshift(fft(fftshift(concatenate((z,a,z)))))
#r = r/sum(coeffs)


#m = len(r)/2

#N = len(r)

#x = (arange(N)-N/2.0)/oversample/16.0

#figure(1)
#clf()
#plot(abs(r[m-128:m+128]), 'b.-', markersize=12)


#r[m-128+ch], r[m-128+ch+256]


##figure(4)
##clf()
##plot(x, abs(r), 'b-')
##plot(x[::16*oversample], abs(r[::16*oversample]), 'bo')
##xlim(0,5)
##ylim(0,.5e-4)

##r = abs(fftshift(fft(fftshift(a))))
##r = r/sum(coeffs)
##N = len(r)
##x = (arange(N)-N/2.0)/16.0

##figure(1)
##plot(x, 20*log10(abs(r)), 'bo', markerfacecolor = 'none', markeredgecolor = 'b', markersize=12)
##xlim(-0.5,0.5)

##figure(2)
##clf()
##plot(x, 20*log10(abs(r)), 'b-')
##plot(x, -92.25*ones_like(x), 'k--')
##legend(['response', '-92.25 dB'])

###plot(x, abs(r), '.-')

##b = a.reshape((16, 1024))
##b1 = b
##correction = 0

##while True:
  ##e = 1.0*sum(b1,axis=0) - round(mean(b)*16)
  ##print sum(abs(e))
  ##if sum(abs(e)) == 0:
    ##break
  ##correction = correction + 0.001*(e/sum(b,axis=0))[newaxis,:]
  ##b1 = b - around(correction*b)

##a1 = b1.flatten()

###a1 = convolve(a, ones(1024)*1.0/1024)[512:-511]

##figure(1)

##r = abs(fftshift(fft(fftshift(concatenate((z,a1,z))))))
##r = r/sum(coeffs)
##N = len(r)
##x = (arange(N)-N/2.0)/oversample/16.0

##plot(x, 20*log10(abs(r)), 'r--')

##figure(4)
##plot(x, abs(r), 'r-')
##plot(x[::16*oversample], abs(r[::16*oversample]), 'ro')
##xlim(0,5)
##ylim(0,.5e-4)

##figure(3)
##clf()
##plot(x, 20*log10(abs(r)), 'r-')
##plot(x, -86.5*ones_like(x), 'k--')
##legend(['response', '-86.5 dB'])

##figure(1)

##r = abs(fftshift(fft(fftshift(a1))))
##r = r/sum(coeffs)
##N = len(r)
##x = (arange(N)-N/2.0)/16.0
##plot(x, 20*log10(abs(r)), 'rx', markersize=12)

##legend(['Original', 'Original Nyquist', 'DC stop', 'DC stop Nyquist'])
##xlabel('Frequency')
##ylabel('Response (dB)')

##xlim(-0.5,0.5)
##ylim(-3,1)


##figure(1)
##savefig('passband.png')
##figure(2)
##savefig('response1.png')
##figure(3)
##savefig('response2.png')
##figure(4)
##savefig('zeros.png')


####plot(x,abs(r), '.-')




