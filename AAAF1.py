Nsb = 1024;     # number of subbands
Nch = 64;       # number of channels per subband

# load filter coefficients and pad with zeros

FIRcoeff = numpy.zeros(Nsb *  Nch )
cf = open("Coeffs16384Kaiser-quant.dat")
for i, coeff in enumerate(cf):
    FIRcoeff[i] = int(coeff)

#FIRspec = abs(fft(FIRcoeff))**2;

#FIRspec = FIRspec * Nch / sum(FIRspec);
#FIRspec = numpy.roll(FIRspec, int(1.5*Nch))[:3*Nch].reshape((3, Nch))

